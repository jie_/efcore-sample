﻿using EFCoreDemo.Entitys.Entitys;
using Microsoft.EntityFrameworkCore;
using System;

namespace EFCoreDemo.Entitys
{
    /// <summary>
    /// 数据访问上下文
    /// </summary>
    public class BlogContext : DbContext
    {
        public BlogContext(DbContextOptions<BlogContext> options)
           : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(message => Console.WriteLine(message));
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<TagEntity> Tags { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ArticleEntity> Articles { get; set; }
        public DbSet<ArticleDetailEntity> ArticleDetails { get; set; }
        //public DbSet<UserArticleEntity> UserArticles{ get; set; }
        public DbSet<ArticleTagEntity> ArticleTags { get; set; }
    }
}
