﻿
using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Entitys.Entitys
{
    /// <summary>
    /// 文章标签
    /// </summary>
    public class ArticleTagEntity : BaseEntity
    {
        /// <summary>
        /// 文章Id
        /// </summary>
        [MaxLength(50)]
        public string ArticleId { get; set; }

        /// <summary>
        /// 标签ID
        /// </summary>
        [MaxLength(50)]
        public string TagId { get; set; }
    }
}
