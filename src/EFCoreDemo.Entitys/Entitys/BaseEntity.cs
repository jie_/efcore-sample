﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Entitys.Entitys
{
    public class BaseEntity
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Key]
        [MaxLength(60)]
        public string Id { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 是否删除
        /// </summary>
        public int IsDel { get; set; } = 0;
    }
}
