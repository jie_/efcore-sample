﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCoreDemo.Entitys.Entitys
{
    [Table("Tags")]
    public class TagEntity : BaseEntity
    {
        [Column("TagName")]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
