﻿using System.ComponentModel.DataAnnotations;

namespace EFCoreDemo.Entitys.Entitys
{
    /// <summary>
    /// 文章详情
    /// </summary>
    public class ArticleDetailEntity : BaseEntity
    {
        /// <summary>
        /// 文章ID
        /// </summary>
        [MaxLength(50)]
        [Required]
        public string ArticleId { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        public string Content { get; set; } 
    }
}
