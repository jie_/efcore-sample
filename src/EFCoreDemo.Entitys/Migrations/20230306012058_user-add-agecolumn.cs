﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCoreDemo.Entitys.Migrations
{
    public partial class useraddagecolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "Users",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ArticleDetails_ArticleId",
                table: "ArticleDetails",
                column: "ArticleId",
                unique: true);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_ArticleDetails_Articles_ArticleId",
            //    table: "ArticleDetails",
            //    column: "ArticleId",
            //    principalTable: "Articles",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticleDetails_Articles_ArticleId",
                table: "ArticleDetails");

            migrationBuilder.DropIndex(
                name: "IX_ArticleDetails_ArticleId",
                table: "ArticleDetails");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Users");
        }
    }
}
