﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCoreDemo.Entitys.Migrations
{
    public partial class ArticleAddUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Tags_Articles_ArticleEntityId",
            //    table: "Tags");

            //migrationBuilder.DropTable(
            //    name: "UserArticles");

            //migrationBuilder.DropIndex(
            //    name: "IX_Tags_ArticleEntityId",
            //    table: "Tags");

            //migrationBuilder.DropColumn(
            //    name: "ArticleEntityId",
            //    table: "Tags");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Articles",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Articles");

            migrationBuilder.AddColumn<string>(
                name: "ArticleEntityId",
                table: "Tags",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserArticles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ArticleId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDel = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserArticles", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tags_ArticleEntityId",
                table: "Tags",
                column: "ArticleEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tags_Articles_ArticleEntityId",
                table: "Tags",
                column: "ArticleEntityId",
                principalTable: "Articles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
