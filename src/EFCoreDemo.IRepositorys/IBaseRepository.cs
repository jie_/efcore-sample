﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EFCoreDemo.IRepositorys
{
    public interface IBaseRepository<T> where T : class, new()
    {
        IQueryable<T> Query();

        IQueryable<T> QueryWithNoLock();

        IQueryable<T> Where(Expression<Func<T, bool>> whereLambda);

        ValueTask<EntityEntry<T>> InsertAsync(T entity);

        Task BatchInserAsync(List<T> entitys);

        void Update(T entity);

        Task<int> BatchUpdateAsync(Expression<Func<T, bool>> whereLambda, Expression<Func<T, T>> entity);

        Task<int> BatchDeleteAsync(Expression<Func<T, bool>> whereLambda);

        Task<bool> IsExist(Expression<Func<T, bool>> whereLambda);

        Task<T> GetEntity(Expression<Func<T, bool>> whereLambda);

        Task<List<T>> Select();

        Task<List<T>> Select(Expression<Func<T, bool>> whereLambda);

        Task<Tuple<List<T>, int>> Select<S>(int pageIndex, int pageSize, Expression<Func<T, bool>> whereLambda, Expression<Func<T, S>> orderByLambda, bool isAsc);
    }
}
