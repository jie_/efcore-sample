﻿using System;
using System.Threading.Tasks;

namespace EFCoreDemo.IRepositorys
{
    public interface IUnitOfWork : IDisposable
    {
        IBaseRepository<TEntity> Repository<TEntity>() where TEntity : class, new(); 

        void BeginTransaction();

        int Commit();

        Task<int> CommitAsync();
    }
}
