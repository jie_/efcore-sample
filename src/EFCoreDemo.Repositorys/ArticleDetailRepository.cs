﻿using EFCoreDemo.Entitys.Entitys;
using EFCoreDemo.IRepositorys;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Repositorys
{
    public class ArticleDetailRepository:BaseRepository<ArticleDetailEntity>, IArticleDetailRepository
    {
        public ArticleDetailRepository(DbContext dbContext) : base(dbContext) { }
    }
}
