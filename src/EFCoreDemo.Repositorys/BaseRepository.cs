﻿using EFCore.BulkExtensions;
using EFCoreDemo.IRepositorys;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EFCoreDemo.Repositorys
{
    public partial class BaseRepository<T> : IBaseRepository<T> where T : class, new()
    {
        private readonly DbContext _dbContext;
        public BaseRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> Query()
        {
            return _dbContext.Set<T>().AsQueryable();
        }

        public IQueryable<T> QueryWithNoLock()
        {
            var sql = _dbContext.Set<T>().ToQueryString() + " WITH (NOLOCK)";
            return _dbContext.Set<T>().FromSqlRaw(sql);
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> whereLambda)
        {
            return _dbContext.Set<T>().Where(whereLambda);
        }

        public async ValueTask<EntityEntry<T>> InsertAsync(T entity)
        {
            return await _dbContext.Set<T>().AddAsync(entity);
        }

        public async Task BatchInserAsync(List<T> entitys)
        {
            await _dbContext.Set<T>().AddRangeAsync(entitys);
        }

        public void Update(T entity)
        {
            _dbContext.Set<T>().Update(entity);
        }

        public async Task<int> BatchUpdateAsync(Expression<Func<T, bool>> whereLambda, Expression<Func<T, T>> entity)
        {
            return await Where(whereLambda).BatchUpdateAsync(entity);
        }

        public async Task<int> BatchDeleteAsync(Expression<Func<T, bool>> whereLambda)
        {
            return await Where(whereLambda).BatchDeleteAsync();
        }

        public async Task<bool> IsExist(Expression<Func<T, bool>> whereLambda)
        {
            return await _dbContext.Set<T>().AnyAsync(whereLambda);
        }

        public async Task<T> GetEntity(Expression<Func<T, bool>> whereLambda)
        {
            return await _dbContext.Set<T>().AsNoTracking().FirstOrDefaultAsync(whereLambda);
        }

        public async Task<List<T>> Select()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<List<T>> Select(Expression<Func<T, bool>> whereLambda)
        {
            return await Where(whereLambda).ToListAsync();
        }

        public async Task<Tuple<List<T>, int>> Select<S>(int pageIndex, int pageSize, Expression<Func<T, bool>> whereLambda, Expression<Func<T, S>> orderByLambda, bool isAsc)
        {
            var query = Where(whereLambda);
            var total = await query.CountAsync();
            var entities = await
                        (isAsc
                        ? query.OrderBy(orderByLambda)
                        : query.OrderByDescending(orderByLambda)
                        )
                        .Skip(pageSize * (pageIndex - 1))
                        .Take(pageSize).ToListAsync();
            return new Tuple<List<T>, int>(entities, total);
        }
    }
}
