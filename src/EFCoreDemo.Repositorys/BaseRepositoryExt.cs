﻿using EFCoreDemo.IRepositorys;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EFCoreDemo.Repositorys
{
    public partial class BaseRepository<T> : IBaseRepository<T> where T : class, new()
    {
        private IQueryable<T> WithNoLock(IQueryable<T> query)
        {
            var a = _dbContext.Set<T>().ToQueryString();

            var dc = query.CreateDbCommand();
            var sql = dc.CommandText + " WITH (NOLOCK)";
            return _dbContext.Set<T>().FromSqlRaw(sql);
        }
    }
}
