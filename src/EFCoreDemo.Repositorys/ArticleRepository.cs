﻿using EFCoreDemo.Entitys.Entitys;
using EFCoreDemo.IRepositorys;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Repositorys
{
    public class ArticleRepository : BaseRepository<ArticleEntity>, IArticelRepository
    {
        public ArticleRepository(DbContext blogContext) : base(blogContext) { }
    }
}
