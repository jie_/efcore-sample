﻿using EFCoreDemo.IRepositorys;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EFCoreDemo.Service
{
    public class BaseService<T> where T : class, new()
    {
        protected IUnitOfWork unitOfWork;
        protected IBaseRepository<T> currentRepository;

        public BaseService(IUnitOfWork unitOfWork, IBaseRepository<T> currentRepository)
        {
            this.unitOfWork = unitOfWork;
            this.currentRepository = currentRepository;
        }

        public async Task<int> InsertAsync(T entity)
        {
            await currentRepository.InsertAsync(entity);
            return await unitOfWork.CommitAsync();
        }

        public async Task BatchInserAsync(List<T> entitys)
        {
            await currentRepository.BatchInserAsync(entitys);
            await unitOfWork.CommitAsync();
        }

        public async Task<int> UpdateAsync(T entity)
        {
            currentRepository.Update(entity);
            return await unitOfWork.CommitAsync();
        }

        public async Task<int> BatchUpdateAsync(Expression<Func<T, bool>> whereLambda, Expression<Func<T, T>> entity)
        {
            await currentRepository.BatchUpdateAsync(whereLambda, entity);
            return await unitOfWork.CommitAsync();
        }

        public async Task<int> BatchDeleteAsync(Expression<Func<T, bool>> whereLambda)
        {
            await currentRepository.BatchDeleteAsync(whereLambda);
            return await unitOfWork.CommitAsync();
        }

        public async Task<bool> IsExist(Expression<Func<T, bool>> whereLambda)
        {
            return await currentRepository.IsExist(whereLambda);
        }

        public async Task<T> GetEntity(Expression<Func<T, bool>> whereLambda)
        {
            return await currentRepository.GetEntity(whereLambda);
        }

        public async Task<List<T>> Select()
        {
            return await currentRepository.Select();
        }

        public async Task<List<T>> Select(Expression<Func<T, bool>> whereLambda)
        {
            return await currentRepository.Select(whereLambda);
        }

        public async Task<Tuple<List<T>, int>> Select<S>(int pageIndex, int pageSize, Expression<Func<T, bool>> whereLambda, Expression<Func<T, S>> orderByLambda, bool isAsc)
        {
            return await currentRepository.Select(pageIndex, pageSize, whereLambda, orderByLambda, isAsc);
        }
    }
}
